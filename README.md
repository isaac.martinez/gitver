# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

# Encapsulated repo
## Different version
### Argo CD

#### Plugins:
 - Very simple setup:
   - install binary(ies) on argocd-repo-server using init-containers
   - plugin has two phases:
     - setup: execute some command to setup the plugin (I used it to calculate the version to use based on the desired version and tags)
     - resource generation: execute some command to generate the resources to be deployed.
   - Dhall:
     - Used dhall to generate the kubernetes resources
     - The first time generating the resources is extremely slow (2-3 minutes), I believe it has to do with importing the whole kubernetes package.
       After first execution, the resources are generated almost immediately (library is being cached). This happened every time the repo-server was restarted.
#### Encapsulated app:
 - 
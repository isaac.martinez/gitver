# Commands
version = scripts/version.sh
gitver = gitver --sky-app-path ./code/test/_data/app-deploy-code.yaml

# Constants
appName = gitver
codeTag = $(appName)-code
deploymentTag = $(appName)-deployment
appTag = $(appName)-app

.PHONY: code
code : docker

.PHONY: docker
docker:
	@semver=$$($(gitver) current code); \
	image=$(appName):$$semver; \
	echo Building image $$image; \
	set -x; \
	docker build -f code/docker/Dockerfile --build-arg CODE_VERSION=$$semver -t $$image .; \
	kind load docker-image --name argo $$image; \

.PHONY: deployment
deployment: % :

.PHONY: app
app: % :

release-%:
	$(MAKE)	tag-$*
	$(MAKE)	$*

tag-%:
	@newTag=$$($(gitver) next $* -tm ); \
	set -x; \
	git tag -a $$newTag -m "'$@ $(date -u)'"; \

.PHONY: push
push :
	git push --tags

# params
PROMOTE_VERSION ?= $(shell $(gitver) current app -t)
.PHONY: promote
promote:
	argocd app set gitver --revision $(PROMOTE_VERSION)

.PHONY: localInstall
localInstall:
	@ver=$$(git rev-parse --short HEAD); \
	pushd code; \
	go install -ldflags "-s -X 'totahuanocotl/gitver/cmd.buildVersion=$$ver' -X 'totahuanocotl/gitver/cmd.buildTime=$(shell date -u)' -linkmode external -extldflags -static" -a -installsuffix static -v; \
	popd; \
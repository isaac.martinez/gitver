-- Importing other files is done by specifying the HTTPS URL/disk location of
-- the file. Attaching a sha256 hash (obtained with `dhall freeze`) allows
-- the Dhall compiler to cache these files and speed up configuration loads
-- drastically.
let kubernetes =
      https://raw.githubusercontent.com/dhall-lang/dhall-kubernetes/master/1.15/package.dhall sha256:d6308415b1c65f0ef752982877155ed2e171c6af9850b7605b561c608c205ad6

let versions = ./versions.dhall

let nginx = "nginx"

let namespace =
      kubernetes.Namespace::{
      , metadata = kubernetes.ObjectMeta::{ name = Some versions.name }
      }

let deployment =
      kubernetes.Deployment::{
      , metadata = kubernetes.ObjectMeta::{
        , name = Some nginx
        , labels = Some
          [ { mapKey = "app.kubernetes.io/name", mapValue = versions.name }
          , { mapKey = "app.kubernetes.io/instance", mapValue = versions.name }
          , { mapKey = "app.kubernetes.io/version", mapValue = versions.app }
          , { mapKey = "app.sky.uk/version.deployment"
            , mapValue = versions.deployment
            }
          , { mapKey = "app.sky.uk/version.app", mapValue = versions.app }
          ]
        }
      , spec = Some kubernetes.DeploymentSpec::{
        , selector = kubernetes.LabelSelector::{
          , matchLabels = Some
            [ { mapKey = "app.kubernetes.io/name", mapValue = versions.name }
            , { mapKey = "app.kubernetes.io/instance"
              , mapValue = versions.name
              }
            ]
          }
        , replicas = Some 2
        , template = kubernetes.PodTemplateSpec::{
          , metadata = kubernetes.ObjectMeta::{
            , name = Some nginx
            , labels = Some
              [ { mapKey = "app.sky.uk/version.code", mapValue = versions.code }
              , { mapKey = "app.kubernetes.io/name", mapValue = versions.name }
              , { mapKey = "app.kubernetes.io/instance"
                , mapValue = versions.name
                }
              ]
            }
          , spec = Some kubernetes.PodSpec::{
            , initContainers = Some
              [ kubernetes.Container::{
                , name = "gitver"
                , image = Some "gitver:${versions.code}"
                , command = Some [ "/bin/dhall/gitver", "--version" ]
                }
              ]
            , containers =
              [ kubernetes.Container::{
                , name = nginx
                , image = Some "${nginx}:1.15.3"
                , ports = Some
                  [ kubernetes.ContainerPort::{ containerPort = 80 } ]
                }
              ]
            }
          }
        }
      }

in  { apiVersion = "v1"
    , kind = "List"
    , items =
      [ kubernetes.Resource.Deployment deployment
      , kubernetes.Resource.Namespace namespace
      ]
    }

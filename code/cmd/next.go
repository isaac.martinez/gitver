/*
Copyright © 2020 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, GetVersion 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"fmt"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"totahuanocotl/gitver/internal/git"
	"totahuanocotl/gitver/internal/sky"
)

const (
	majorFlag = "major"
	minorFlag = "minor"
	patchFlag = "patch"
)

var (
	nextMajor = false
	nextMinor = false
	nextPatch = false
)

func init() {
	nextCmd.AddCommand(nextAppCmd)
	nextCmd.AddCommand(nextCodeCmd)
	nextCmd.AddCommand(nextDeploymentCmd)
	rootCmd.AddCommand(nextCmd)

	nextCmd.PersistentFlags().BoolVarP(&nextMajor, majorFlag, "M", false, "increases major and resets minor and patch. Ignores -mp")
	nextCmd.PersistentFlags().BoolVarP(&nextMinor, minorFlag, "m", false, "increases minor and resets patch. Ignores -p")
	nextCmd.PersistentFlags().BoolVarP(&nextPatch, patchFlag, "p", false, "increases patch")
	nextCmd.PersistentFlags().BoolVarP(&asTag, asTagFlag, "t", false, "output as tag")

}

// nextCmd retrieves the versions for the modules
var nextCmd = &cobra.Command{
	Use:               "next",
	Short:             "Calculate modules' next Version tags from git",
	Long:              `A poorman's implementation of git currentVersion. Returns nextAll modules: app, code, deployment`,
	PersistentPreRunE: loadAppDefinition,
	Run:               nextAll,
}

// nextAppCmd represents the currentApp command
var nextAppCmd = &cobra.Command{
	Use:   "app",
	Short: "Calculate the next Version of app module",
	Long:  `A poorman's implementation of git currentVersion.`,
	Run:   nextApp,
}

// nextCodeCmd represents the currentCode command
var nextCodeCmd = &cobra.Command{
	Use:   "code",
	Short: "Calculate the next Version of code module",
	Long:  `A poorman's implementation of git currentVersion.`,
	Run:   nextCode,
}

// nextDeploymentCmd represents the currentDeployment command
var nextDeploymentCmd = &cobra.Command{
	Use:   "deployment",
	Short: "Calculate the next Version of deployment module",
	Long:  `A poorman's implementation of git currentVersion.`,
	Run:   nextDeployment,
}

func nextAll(_ *cobra.Command, _ []string) {
	code, err := nextVersion(skyApp.Spec.ConfigSource.Code)
	if err != nil {
		logrus.Error(err)
	}
	deployment, err := nextVersion(skyApp.Spec.ConfigSource.Deployment)
	if err != nil {
		logrus.Error(err)
	}
	app, err := nextVersion(skyApp.Spec.ConfigSource.App)
	if err != nil {
		logrus.Error(err)
	}

	switch output {
	case sky.Text:
		fmt.Printf("name: %s\napp: %s\ncode: %s\ndeployment: %s", skyApp.Metadata.Name, app, code, deployment)
	case sky.Dhall:
		fmt.Printf("{ name = %q, app = %q, code = %q, deployment = %q }", skyApp.Metadata.Name, app, code, deployment)
	}
}

func nextApp(_ *cobra.Command, _ []string)  {
	if ver, err:= nextVersion(skyApp.Spec.ConfigSource.App); err != nil {
		logrus.Error(err)
	} else {
		fmt.Println(ver)
	}
}

func nextCode(_ *cobra.Command, _ []string)  {
	if ver, err:= nextVersion(skyApp.Spec.ConfigSource.Code); err != nil {
		logrus.Error(err)
	} else {
		fmt.Println(ver)
	}
}

func nextDeployment(_ *cobra.Command, _ []string)  {
	if ver, err:= nextVersion(skyApp.Spec.ConfigSource.Deployment); err != nil {
		logrus.Error(err)
	} else {
		fmt.Println(ver)
	}
}

func nextVersion(source sky.Source) (string, error) {
	version, err := git.GetVersion(source.TagPrefix, source.Path)
	if err != nil {
		return "", errors.Wrap(err, fmt.Sprintf("current  tags for prefix %q", source.TagPrefix))
	}
	logrus.Infof("Version: %s", version.String())
	if asTag {
		return version.NextTag(nextMajor, nextMinor, nextPatch), nil
	}
	return version.NextVersion(nextMajor, nextMinor, nextPatch), nil
}

/*
Copyright © 2020 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, GetVersion 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"fmt"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"totahuanocotl/gitver/internal/git"
	"totahuanocotl/gitver/internal/sky"
)

const (
	asTagFlag ="as-tag"
)

var (
	asTag = false
)

// currentCmd retrieves the versions for the modules
var currentCmd = &cobra.Command{
	Use:               "current",
	Short:             "Retrieve modules' current version tags from git",
	Long:              `A poorman's implementation of git currentVersion. Returns nextAll modules: app, code, deployment`,
	PersistentPreRunE: loadAppDefinition,
	Run:               currentAll,
}

// currentAppCmd represents the currentApp command
var currentAppCmd = &cobra.Command{
	Use:   "app",
	Short: "Retrieve the app version of",
	Long:  `A poorman's implementation of git version.`,
	Run:   currentApp,
}

// currentCodeCmd represents the currentCode command
var currentCodeCmd = &cobra.Command{
	Use:   "code",
	Short: "Retrieve the Code version",
	Long:  `A poorman's implementation of git version.`,
	Run:   currentCode,
}

// currentDeploymentCmd represents the currentDeployment command
var currentDeploymentCmd = &cobra.Command{
	Use:   "deployment",
	Short: "Retrieve theDeployment version",
	Long:  `A poorman's implementation of git version.`,
	Run:   currentDeployment,
}

func init() {
	currentCmd.AddCommand(currentAppCmd)
	currentCmd.AddCommand(currentCodeCmd)
	currentCmd.AddCommand(currentDeploymentCmd)
	rootCmd.AddCommand(currentCmd)
	currentCmd.PersistentFlags().BoolVarP(&asTag, asTagFlag, "t", false, "output as tag")
}

func currentAll(_ *cobra.Command, _ []string) {
	code, err := currentVersion(skyApp.Spec.ConfigSource.Code)
	if err != nil {
		logrus.Error(err)
	}
	deployment, err := currentVersion(skyApp.Spec.ConfigSource.Deployment)
	if err != nil {
		logrus.Error(err)
	}
	app, err := currentVersion(skyApp.Spec.ConfigSource.App)
	if err != nil {
		logrus.Error(err)
	}

	switch output {
	case sky.Text:
		fmt.Printf("name: %s\ncurrentApp: %s\ncurrentCode: %s\ncurrentDeployment: %s", skyApp.Metadata.Name, app, code, deployment)
	case sky.Dhall:
		fmt.Printf("{ name = %q, currentApp = %q, currentCode = %q, currentDeployment = %q }", skyApp.Metadata.Name, app, code, deployment)
	}
}

func currentApp(_ *cobra.Command, _ []string)  {
	if ver, err:= currentVersion(skyApp.Spec.ConfigSource.App); err != nil {
		logrus.Error(err)
	} else {
		fmt.Println(ver)
	}
}

func currentCode(_ *cobra.Command, _ []string)  {
	if ver, err:= currentVersion(skyApp.Spec.ConfigSource.Code); err != nil {
		logrus.Error(err)
	} else {
		fmt.Println(ver)
	}
}

func currentDeployment(_ *cobra.Command, _ []string)  {
	if ver, err:= currentVersion(skyApp.Spec.ConfigSource.Deployment); err != nil {
		logrus.Error(err)
	} else {
		fmt.Println(ver)
	}
}

func currentVersion(source sky.Source) (string, error) {
	version, err := git.GetVersion(source.TagPrefix, source.Path)
	if err != nil {
		return "", errors.Wrap(err, fmt.Sprintf("currentVersion tags for prefix %q", source.TagPrefix))
	}
	logrus.Infof("Version: %#v", version)
	if asTag {
		return version.Tag(), nil
	}
	return version.Version(), nil
}

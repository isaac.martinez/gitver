/*
Copyright © 2020 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, GetVersion 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"fmt"
	"github.com/google/martian/log"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"totahuanocotl/gitver/internal/sky"
)

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:               "gitver",
	Short:             "Retrieve currentVersion tags from git",
	Long:              `A poorman's implementation of git currentVersion.`,
	PersistentPreRunE: loadAppDefinition,
}

// Execute adds nextAll child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := rootCmd.Execute(); err != nil {
	}
}

const (
	skyAppPathFlag = "sky-app-path"
	outputFlag     = "output"
	debugFlag      = "debug"
)

var (
	// injected by "go tool link -X"
	buildVersion string
	buildTime    string
)

var (
	debug      bool
	skyAppPath string
	skyApp     sky.Application
	output     sky.Output
)

func init() {
	cobra.OnInitialize(initLogs)
	rootCmd.Version = fmt.Sprintf("%s (%s)", buildVersion, buildTime)
	rootCmd.PersistentFlags().BoolVarP(&debug, debugFlag, "x", false, "enables debug logs")
	rootCmd.PersistentFlags().StringVarP(&skyAppPath, skyAppPathFlag, "a", "", "sky currentApp definition file path (Required)")
	rootCmd.PersistentFlags().StringVarP((*string)(&output), outputFlag, "o", sky.Text, "sky currentApp definition file path (Required)")
	_ = rootCmd.MarkPersistentFlagRequired(skyAppPath)
}

func initLogs() {
	formatter:= new(logrus.TextFormatter)
	formatter.FullTimestamp = true
	logrus.SetFormatter(formatter)
	if debug {
		logrus.SetLevel(logrus.DebugLevel)
	}
}

func loadAppDefinition(_ *cobra.Command, _ []string) error {
	var err error
	skyApp, err = sky.LoadFromDefinition(skyAppPath)
	if err != nil {
		return errors.Wrap(err, "init command")
	}
	log.Infof("Loaded app definition: %v", skyApp)
	return nil
}

module totahuanocotl/gitver

go 1.13

require (
	github.com/coreos/go-semver v0.3.0
	github.com/google/martian v2.1.0+incompatible
	github.com/onsi/gomega v1.10.3
	github.com/pkg/errors v0.8.1
	github.com/sirupsen/logrus v1.2.0
	github.com/spf13/cobra v1.1.1
	gopkg.in/yaml.v2 v2.3.0
)

package sky

import (
	"github.com/pkg/errors"
	"gopkg.in/yaml.v2"
	"io/ioutil"
)

type Output string

func (s *Output) Set(val string) error {
	*s = Output(val)
	return nil
}

func (s *Output) Type() string {
	return "Output"
}

func (s *Output) String() string { return string(*s) }

const(
	Dhall Output = "dhall"
	Text ="text"
)

type Application struct {
	Version  string   `yaml:"version"`
	Kind     string   `yaml:"kind"`
	Metadata Metadata `yaml:"metadata"`
	Spec     Spec     `yaml:"spec"`
}

type Spec struct {
	ConfigSource ConfigSource `yaml:"configSource"`
}

type Metadata struct {
	Name string `yaml:"name"`
}

type Source struct {
	Repository string `yaml:"repository"`
	Path       string `yaml:"path"`
	TagPrefix  string `yaml:"tagPrefix"`
}

type ConfigSource struct {
	App        Source `yaml:"app"`
	Code       Source `yaml:"code"`
	Deployment Source `yaml:"deployment"`
}

func LoadFromDefinition(skyAppPath string) (Application, error) {
	var app Application
	data, err := ioutil.ReadFile(skyAppPath)
	if err != nil {
		return app, errors.Wrap(err, "read file")
	}
	err = yaml.Unmarshal(data, &app)
	if err != nil {
		return app, errors.Wrap(err, "unmarshal yaml")
	}
	return app, nil
}

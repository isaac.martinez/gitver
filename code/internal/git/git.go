package git

import (
	"bufio"
	"fmt"
	"github.com/coreos/go-semver/semver"
	"github.com/pkg/errors"
	"regexp"
	"strings"
)

type Version struct {
	prefix string
	version *semver.Version
	distance int
	revision string
	onMaster bool

}

func (v *Version) String() string {
	return fmt.Sprintf("{prefix: %q, version:%q, distance: %d, revision: %q, onMaster: %t}", v.prefix, v.version.String(), v.distance, v.revision, v.onMaster)
}


func (v *Version) Version() string {
	if v.onMaster {
		return v.version.String()
	}
	return v.revision
}

func (v *Version) Tag() string {
	if v.onMaster {
		return fmt.Sprintf("%s-%s", v.prefix, v.version.String())
	}
	return v.revision
}

func (v *Version) NextVersion(major, minor, patch bool) string {
	next := semver.New(v.version.String())
	if major {
		next.BumpMajor()
	} else if minor {
		next.BumpMinor()
	} else if patch {
		next.BumpPatch()
	}
	return next.String()
}

func (v *Version) NextTag(major, minor, patch bool) string {
	if v.onMaster {
		return fmt.Sprintf("%s-%s", v.prefix, v.NextVersion(major, minor, patch))
	}
	return v.revision
}

// GetVersion returns the version of the tag prefix
func GetVersion(tagPrefix, repoDir string) (*Version, error) {
	describe := describe(tagPrefix, repoDir)
	if err := describe.Run(); err != nil && !isUntagged(describe.Err()) {
		return nil, errors.Wrap(err, fmt.Sprintf("version tags for prefix %q in repo %q", tagPrefix, repoDir))
	}
	return describeToVersion(tagPrefix, describe.Out(), repoDir)
}

func isUntagged(errorMessage string) bool {
	return strings.Contains(errorMessage, "No names found")
}

func describe(tagPrefix, repoDir string) *GitWrapper {
	match := fmt.Sprintf("--match=%s-*", tagPrefix)
	cmd := newCommand("describe", "--tags", "--abbrev=0", match)
	cmd.SetWorkingDir(repoDir)
	return cmd
}

func describeToVersion(tagPrefix, description, repoDir string) (*Version, error) {
	var err error
	var tag, semantic, tagRev, headRev string
	version := &Version{
		prefix:   tagPrefix,
	}
	if description != "" {
		r := regexp.MustCompile(fmt.Sprintf("(?P<prefix>%s)-(?P<semver>.+)", tagPrefix))
		groups := r.FindStringSubmatch(description)
		if groups == nil {
			return nil, fmt.Errorf("invalid 'git describe' output: %q", description)
		}
		tag = groups[0]
		semantic = groups[2]
		tagRev, err = revision(tag, repoDir)
		if err != nil {
			return nil, err
		}
	}
	headRev, err = revision("HEAD", repoDir)
	if err != nil {
		return nil, err
	}
	hasChanged, err := hasChanged(tagRev, headRev, repoDir)
	if err != nil {
		return nil, err
	}
	if hasChanged {
		version.revision=headRev
	} else {
		version.revision = tagRev
	}
	version.onMaster, err = isOnMaster(version.revision, repoDir)
	if err != nil {
		return nil, errors.Wrap(err, "on master")
	}
	if semantic != "" {
		version.version = semver.New(semantic)
	} else {
		version.version = semver.New("0.0.0")
	}
	return version, nil
}

func hasChanged(tag, head, repoDir string) (bool, error) {
	cmd := newCommand("diff", fmt.Sprintf("%s..%s", tag, head), ".")
	cmd.SetWorkingDir(repoDir)
	if err := cmd.Run(); err != nil {
		return false, errors.Wrap(err, fmt.Sprintf("diff between  %q and %q in repo %q", tag, head, repoDir))
	}
	changes := cmd.Out()
	return changes !="", nil
}

func isOnMaster(revision, repoDir string) (bool, error) {
	cmd := branchContains(revision, repoDir)
	if err := cmd.Run(); err != nil {
		return false,  errors.Wrap(err, fmt.Sprintf("branch contains for revision %q in repo %q", revision, repoDir))
	}
	branches := bufio.NewScanner(strings.NewReader(cmd.Out()))
	for branches.Scan() {
		if strings.TrimSpace(branches.Text()) == "origin/master" {
			return true, nil
		}
	}
	return false, nil
}

func branchContains(revision string, repoDir string) *GitWrapper {
	cmd := newCommand("branch", "-r", "--contains", revision)
	cmd.SetWorkingDir(repoDir)
	return cmd
}

func revision(ref string, repoDir string) (string, error) {
	cmd := newCommand("rev-parse", ref)
	cmd.SetWorkingDir(repoDir)
	if err := cmd.Run(); err != nil {
		return "", errors.Wrap(err, fmt.Sprintf("revision for ref %q in repo %q", ref, repoDir))
	}
	return cmd.Out(), nil
}


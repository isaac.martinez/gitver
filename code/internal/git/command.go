package git

import (
	"bytes"
	"fmt"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"os"
	"os/exec"
	"path"
	"strings"
)



// newCommand returns an instance of a TestCommand
func newCommand(args ...string) *GitWrapper {
	return &GitWrapper{Name: "git", Args: args}
}

// GitWrapper wraps an OS process to be ran synchronously.
type GitWrapper struct {
	Name  string
	Args  []string
	Env   []string
	cmd   *exec.Cmd
	dir   string
	error error
	out   bytes.Buffer
	err   bytes.Buffer
}

func (c *GitWrapper) SetWorkingDir(repoDir string) {
	c.dir = repoDir
}

func (c *GitWrapper) SetEnvironment(entries map[string]string) {
	var env []string
	for k, v := range entries {
		env = append(env, fmt.Sprintf("%s=%s", k, v))
	}
	c.Env = env
}

func (c *GitWrapper) Run() error {
	c.cmd = exec.Command(c.Name, c.Args...)
	return c.run()
}

func (c *GitWrapper) Successful() bool {
	return c.error == nil
}

func (c *GitWrapper) Failed() bool {
	return !c.Successful()
}

func (c *GitWrapper) Out() string {
	return strings.TrimSpace(string(c.out.Bytes()))
}

func (c *GitWrapper) Err() string {
	return strings.TrimSpace(string(c.err.Bytes()))
}

func (c *GitWrapper) CombinedOutput() string {
	return fmt.Sprintf("%s\n%s", c.Out(), c.Err())
}

func (c *GitWrapper) PrintOutput() {
	fmt.Println("--- Output ---")
	fmt.Println(c.cmd)
	fmt.Printf(c.CombinedOutput())
	fmt.Println("--- End Output ---")
}

func (c *GitWrapper) Error() error {
	return errors.New(strings.TrimSpace(c.Err()))
}

func (c *GitWrapper) run() error {
	c.cmd.Env = c.Env
	cw, err := os.Getwd()
	if err != nil {
		return errors.Wrap(err, "get working dir")
	}
	c.cmd.Dir = path.Join(cw,c.dir)
	c.cmd.Stdout = &c.out
	c.cmd.Stderr = &c.err
	c.error = c.cmd.Run()
	logrus.Debugf("Git Command: %s\nWD: %s\nGit Out: %s\nGit Err: %s", c.Args, c.dir, c.Out(), c.Err())
	return c.error
}
